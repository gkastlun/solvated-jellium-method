
"""Various tools for doing jellium calculations."""

from ase.units import Bohr


def write_parallel_func_in_z(gd, g, name='g_z.out'):
        from gpaw.mpi import world
        G = gd.collect(g, broadcast=False)
        # G = gd.zero_pad(G,global_array=False)
        if world.rank == 0:
            G_z = G.mean(0).mean(0)
            out = open(name, 'w')
            for i, val in enumerate(G_z):
                out.writelines('%f  %1.8f\n' % ((i + 1) * gd.h_cv[2][2] * Bohr,
                               val))
            out.close()


def write_parallel_func_on_grid(gd, g, atoms=None, name='func.cube',
                                outstyle='cube'):
        from ase.io import write
        G = gd.collect(g, broadcast=False)
        # G = gd.zero_pad(G)
        if outstyle == 'cube':
            write(name, atoms, data=G)
        elif outstyle == 'pckl':
            import pickle
            out = open(name, 'wb')
            pickle.dump(G, out)
            out.close()
