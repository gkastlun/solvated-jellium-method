#!/bin/bash

export gdebug="/users/gkastlun/.local/bin/gpaw-squeue -c 4 -t 00:59:00 -q debug"

$gdebug run.py
$gdebug run_nopot.py
$gdebug run_cavitylike.py
$gdebug run_kpt.py
$gdebug run_relax.py
$gdebug run_cavitylike_relax.py
