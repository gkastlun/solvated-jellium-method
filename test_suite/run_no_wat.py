import sys,os
sys.path.insert(1,'../')
#sys.path.insert(1,'/gpfs_home/gkastlun/data/gkastlun/opt/gpaw-devel_update')
#sys.path.insert(1,'/gpfs_home/gkastlun/data/gkastlun/opt/gpaw-devel_update/build/lib.linux-x86_64-2.7')
#sys.path.insert(1,'/gpfs_home/gkastlun/data/gkastlun/opt/ase_update/ase')
#from cavity import SJM_EffectivePotentialCavity, SJM_Power12Potential
from cavity import  SJM_Power12Potential
from SJM import SJM
from SJM_tools import write_parallel_func_in_z

from ase.visualize import view
from gpaw import FermiDirac
from ase.io import read
#from ase.units import mol, kJ, kcal, Pascal, m, Bohr
#Import solvation modules
from ase.data.vdw import vdw_radii
from gpaw.solvation import (
    SolvationGPAW,EffectivePotentialCavity,
    LinearDielectric,
    GradientSurface,
    SurfaceInteraction
)
from ase.build import fcc111
#Solvent parameters
u0=0.180 #eV
epsinf = 78.36 #dielectric constant of water at 298 K
gamma =0.00114843767916 #     18.4*1e-3 * Pascal*m
T=298.15   #K
atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]

#atoms=read('test_system.traj')
atoms=fcc111('Au',size=(1,1,3))
atoms.center(axis=2,vacuum=10)
atoms.translate([0,0,-3])

#view(atoms)
#das

potential=3.0
#ne=0.0942430698461
ne=-0.01
#ne=0.0
#ne=0.3

def calculator():
    return SJM(  symmetry={'do_not_symmetrize_the_density': True},
                 doublelayer={'upper_limit':20.},
                 potential=potential,
                 dpot=0.01,
                 ne=ne,
                 verbose=True,

	         gpts =  (16, 16, 160),
                 #h=0.3,
                 poissonsolver={'dipolelayer':'xy'},
                 kpts = (1,1,1),
	         xc = 'PBE',
                 mode='lcao',
                 basis='szp(dzp)',
                 txt='out_no_wat.txt',
	         spinpol = False,
                 maxiter=1000,
	         occupations = FermiDirac(0.1),
                 cavity = EffectivePotentialCavity (
                     effective_potential = SJM_Power12Potential (atomic_radii, u0),#,H2O_layer=True),
                     temperature=T,
                     surface_calculator=GradientSurface ()),
                 #convergence={'energy': 0.005,  # eV / electron
                 #     'density': 1.0e-3,
                 #      'eigenstates': 4.0e-5,  # eV^2 / electron
                 #       'bands': 'occupied',
                 #        'forces': float('inf') # eV / Ang Max
                 #        },
                 dielectric = LinearDielectric (epsinf=epsinf),
                 interactions = [SurfaceInteraction (surface_tension=gamma)])

atoms.calc=calculator()
atoms.calc.atoms=atoms
atoms.get_potential_energy()
atoms.get_forces()
#write_parallel_func_in_z(atoms.calc.density.finegd,atoms.calc.hamiltonian.cavity.g_g,name='cavity.out')

