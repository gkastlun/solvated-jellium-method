import sys,os
#sys.path.append('/users/gkastlun/data/ONR_scripts/tools/standalone')
sys.path.insert(1,'../')
from cavity import SJM_Power12Potential
from SJM import SJM

#from ase.visualize import view
from gpaw import FermiDirac
from ase.io import read
from ase.units import mol, kJ, kcal, Pascal, m, Bohr
#Import solvation modules
from ase.data.vdw import vdw_radii
from ase.io import write
from gpaw.solvation import (
    SolvationGPAW,
    EffectivePotentialCavity,
    LinearDielectric,
    GradientSurface,
    SurfaceInteraction
)

#Solvent parameters
u0=0.180 #eV
epsinf = 78.36 #dielectric constant of water at 298 K
gamma =0.00114843767916 #     18.4*1e-3 * Pascal*m
T=298.15   #K
atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]

atoms=read('test_system.traj')
potential=4.5
ne=0.1

calc= SJM(
                 doublelayer={'start':'cavity_like','upper_limit':14.},
                 #doublelayer={'start':12.5,'upper_limit':14.},
                 potential=potential,
                 dpot=0.01,
                 ne=ne,

	         #gpts =  (48, 32, 88),
	         gpts =  (24, 16, 48),
                 poissonsolver={'dipolelayer':'xy'},
                 kpts = (1,1,1),
	         xc = 'PBE',
                 txt='out_cavitylike.txt',
	         spinpol = False,
                 maxiter=1000,
                 convergence={'energy': 0.005,  # eV / electron
                                'density': 1.0e-4,
                                'eigenstates': 4.0e-8,  # eV^2 / electron
                                'bands': 'occupied',
                                'forces': float('inf')}, # eV / Ang Max
	         occupations = FermiDirac(0.1),
                 cavity = EffectivePotentialCavity (
                     effective_potential = SJM_Power12Potential (atomic_radii, u0,H2O_layer=True),
                     temperature=T,
                     surface_calculator=GradientSurface ()),
                 dielectric = LinearDielectric (epsinf=epsinf),
                 interactions = [SurfaceInteraction (surface_tension=gamma)])

atoms.calc=calc
atoms.calc.atoms=atoms
atoms.get_potential_energy()
sys.exit()
for ne in [1,2]:
     calc.set(ne=ne)
     calc.set(txt='out.txt')
#     atoms.calc=calc
#     atoms.calc.atoms=atoms
     atoms.get_potential_energy()
     rho = atoms.calc.get_all_electron_density(gridrefinement=2)*Bohr**3
     write('%s_density.cube'%(ne), atoms, data=rho)

