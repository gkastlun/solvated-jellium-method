# General imports
import os
import numbers
# ASE and GPAW imports
from ase.calculators.calculator import Calculator
from ase.units import Bohr, Hartree
from gpaw.output import print_cell
from gpaw.forces import calculate_forces
from gpaw.stress import calculate_stress
from gpaw import __version__ as gpaw_version

# Implicit solvent imports
from gpaw.solvation import SolvationGPAW
from gpaw.jellium import JelliumSlab

# SJM specific imports
from jellium import CavityShapedJellium
from hamiltonian import SJM_RealSpaceHamiltonian
from SJM_tools import write_parallel_func_in_z


class SJM(SolvationGPAW):
    """Subclass of the SolvationGPAW class which includes the
       Solvated Jellium method.

       The method allows the simulation of an electrochemical environment
       by calculating constant potential quantities on the basis of constant
       charge DFT runs. For this purpose, it allows the usage of non-neutral
       periodic slab systems. Cell neutrality is achieved by adding a
       background charge in the solvent region above the slab

       Further detail are given in ADD PAER REFERENCE

       Parameters
       ----------

        ne: float
            Number of electrons added in the atomic system and (with opposite
            sign) in the background charge region. At the start it can be an
            initial guess for the needed number of electrons and will be
            changed to the current number in the course of the calculation
        potential: float
            The potential that should be reached or kept in the course of the
            calculation. If set to "None" (default) a constant charge charge
            calculation based on the value of `ne` is performed.
        dpot: float
            Tolerance for the deviation of the input `potential`. If the
            potential is outside the defined range `ne` will be changed in
            order to get inside again.
        doublelayer: dict
            Parameters regarding the shape of the counter charge region
            Implemented keys:
            'start': float or 'cavity_like'
                If a float is given it corresponds to the lower
                boundary coordinate (default: z), where the counter charge
                starts. If 'cavity_like' is given the counter charge will
                take the form of the cavity up to the 'upper_limit'.
            'thickness': float
                Thickness of the counter charge region in Angstrom.
                Can only be used if start is not 'cavity_like' and will
                be overwritten by 'upper_limit'
            'upper_limit': float
                Upper boundary of the counter charge region in terms of
                coordinate in Anfstrom (default: z). The default is
                atoms.cell[2][2] - 5


    """
    implemented_properties = ['energy', 'forces', 'stress', 'dipole',
                              'magmom', 'magmoms', 'ne', 'electrode_potential']

    def __init__(self, ne=0, doublelayer={}, potential=None,
                 dpot=0.01, tiny=1e-8, verbose=False, **gpaw_kwargs):

        self.tiny = tiny
        if abs(ne) < self.tiny:
            self.ne = self.tiny
        else:
            self.ne = ne

        self.potential = potential
        self.dpot = dpot
        self.dl = doublelayer
        self.verbose = verbose
        self.previous_ne = 0

        SolvationGPAW.__init__(self, **gpaw_kwargs)

        self.log('-----------\nGPAW SJM module in %s\n----------\n'
                 % (os.path.abspath(__file__)))

    def create_hamiltonian(self, realspace, mode, xc):
        if not realspace:
            raise NotImplementedError(
                'SJM does not support '
                'calculations in reciprocal space yet.')

        dens = self.density

        if gpaw_version > '1.3.0b1':
            self.hamiltonian = SJM_RealSpaceHamiltonian(
                *self.stuff_for_hamiltonian,
                gd=dens.gd, finegd=dens.finegd,
                nspins=dens.nspins,
                collinear=dens.collinear,
                setups=dens.setups,
                timer=self.timer,
                xc=xc,
                world=self.world,
                redistributor=dens.redistributor,
                vext=self.parameters.external,
                psolver=self.parameters.poissonsolver,
                stencil=mode.interpolation)

        else:
            self.hamiltonian = SJM_RealSpaceHamiltonian(
                *self.stuff_for_hamiltonian,
                gd=dens.gd, finegd=dens.finegd,
                nspins=dens.nspins,
                setups=dens.setups,
                timer=self.timer,
                xc=xc,
                world=self.world,
                redistributor=dens.redistributor,
                vext=self.parameters.external,
                psolver=self.parameters.poissonsolver,
                stencil=mode.interpolation)

        self.log(self.hamiltonian)

    def set(self, **kwargs):
        """Change parameters for calculator.

        It differs from the standard `set` function in the sense, that it
        does not completely reinitialize and delete `self.wfs` if the
        background charge is changed

        Examples::

            calc.set(xc='PBE')
            calc.set(nbands=20, kpts=(4, 1, 1))
        """

        changed_parameters = Calculator.set(self, **kwargs)

        # We need to handle txt early in order to get logging up and running:
        if 'txt' in changed_parameters:
            self.log.fd = changed_parameters.pop('txt')

        if not changed_parameters:
            return {}

        major_changes = False
        for key in changed_parameters:
            if key not in ['background_charge', 'ne']:
                self.initialized = False
                self.scf = None
                self.log('Input parameters:')
                self.log.print_dict(changed_parameters)
                self.log()
                major_changes = True
                break

        self.results = {}

        for key in changed_parameters:
            if key in ['eigensolver', 'convergence'] and self.wfs:
                self.wfs.set_eigensolver(None)

            if key in ['mixer', 'verbose', 'txt', 'hund', 'random',
                       'eigensolver', 'idiotproof']:
                continue

            if key in ['convergence', 'fixdensity', 'maxiter']:
                continue
            if key in ['potential']:
                continue

            if key in ['dpot']:
                self.density = None
            # More drastic changes:
            if self.wfs:
                self.wfs.set_orthonormalized(False)
            if key in ['external', 'xc', 'poissonsolver', 'doublelayer']:
                self.hamiltonian = None
            elif key in ['occupations', 'width']:
                pass
            elif key in ['charge']:
                self.hamiltonian = None
                self.density = None
                self.wfs = None

            elif key in ['kpts', 'nbands', 'symmetry']:
                self.wfs = None
            elif key in ['h', 'gpts', 'setups', 'spinpol', 'dtype', 'mode']:
                self.density = None
                self.hamiltonian = None
                self.wfs = None
            elif key in ['basis']:
                self.wfs = None

            # SJM custom `set` for background charge and ne
            elif key in ['background_charge']:
                self.log('------------')
                if self.wfs is not None:
                    if major_changes:
                        self.density = None

                    else:
                        self.density.reset()

                        self.density.background_charge = \
                            changed_parameters['background_charge']
                        self.density.background_charge.set_grid_descriptor(
                            self.density.finegd)

                        self.spos_ac = self.atoms.get_scaled_positions() % 1.0
                        self.initialize_positions(self.atoms)
                        self.wfs.initialize(self.density, self.hamiltonian,
                                            self.spos_ac)
                        self.wfs.eigensolver.reset()
                        self.scf.reset()

                        self.log('\n------------')
                        self.log('Charge on atomic system is changed!')

                    if abs(self.ne) < self.tiny:
                        self.ne = self.tiny
                    self.wfs.nvalence += self.ne - self.previous_ne
                self.log('Current number of Excess Electrons: %1.4f' % self.ne)
                self.log('------------\n')

            elif key in ['ne']:
                self.previous_ne = self.ne

                if abs(changed_parameters['ne']) < self.tiny:
                    self.ne = self.tiny
                else:
                    self.ne = changed_parameters['ne']

            else:
                raise TypeError('Unknown keyword argument: "%s"' % key)

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=['cell'], ):
        """
        Perform a calculation with SJM

        This module includes the potential equilibration loop characteristic
        for SJM.

        """
        Calculator.calculate(self, atoms)
        atoms = self.atoms
        if system_changes:
            self.log('System changes:', ', '.join(system_changes), '\n')
            if system_changes == ['positions']:
                # Only positions have changed:
                self.density.reset()
            else:
                # Drastic changes:
                self.wfs = None
                self.occupations = None
                self.density = None
                self.hamiltonian = None
                self.scf = None
                self.initialize(atoms)

            self.set_positions(atoms)

        if self.potential:
            for dummy in range(10):
                self.set_jellium(atoms)
                self.calculate2(atoms)
                pot_int = self.get_electrode_potential()

                if abs(pot_int - self.potential) < self.dpot:
                    self.last_pot = pot_int
                    self.previous_ne = self.ne
                    break

                try:
                    if abs(self.previous_ne - self.ne) > 2*self.tiny:
                        self.slope = (pot_int-self.last_pot) / \
                            (self.ne-self.previous_ne)

                    d = (pot_int-self.potential) - (self.slope*self.ne)
                    self.previous_ne = self.ne
                    self.ne = - d / self.slope
                except AttributeError:
                    self.previous_ne = self.ne
                    self.ne += (
                        pot_int - self.potential) / \
                        abs(pot_int - self.potential) * self.dpot
                self.last_pot = pot_int
            else:
                raise Exception(
                    'Potential could not be reached after ten iterations.\
                    Aborting!')

        else:
            self.set_jellium(atoms)
            self.calculate2(atoms)
            self.previous_ne = self.ne

        if 'forces' in properties:
            with self.timer('Forces'):
                F_av = calculate_forces(self.wfs, self.density,
                                        self.hamiltonian, self.log)
                self.results['forces'] = F_av * (Hartree / Bohr)

        if 'stress' in properties:
            with self.timer('Stress'):
                try:
                    stress = calculate_stress(self).flat[[0, 4, 8, 5, 2, 1]]
                except NotImplementedError:
                    # Our ASE Calculator base class will raise
                    # PropertyNotImplementedError for us.
                    pass
                else:
                    self.results['stress'] = stress * (Hartree / Bohr**3)

        if 'electronegativity' in properties or \
           'electrode_potential' in properties:
            electronegativity = self.get_electrode_potential()
            self.results['electronegativity'] = electronegativity

    def calculate2(self, atoms=None):
        """Calculate things."""
        if not self.initialized:
            self.initialize(atoms)
            self.set_positions(atoms)

        if not (self.wfs.positions_set and self.hamiltonian.positions_set):
            self.set_positions(atoms)

        if self.verbose:
            write_parallel_func_in_z(self.density.finegd,
                                     self.hamiltonian.cavity.g_g,
                                     name='cavity.out')

            try:
                write_parallel_func_in_z(self.density.finegd,
                                         self.density.background_charge.mask_g,
                                         name='background_charge.out')
            except AttributeError:
                pass

        if not self.scf.converged:
            print_cell(self.wfs.gd, self.atoms.pbc, self.log)

            with self.timer('SCF-cycle'):
                self.scf.run(self.wfs, self.hamiltonian,
                             self.density, self.occupations,
                             self.log, self.call_observers)

            self.log('\nConverged after {0} iterations.\n'
                     .format(self.scf.niter))

            omega_extra = Hartree * self.hamiltonian.e_el_extrapolated + \
                self.get_electrode_potential() * self.ne
            omega_free = Hartree * self.hamiltonian.e_el_free + \
                self.get_electrode_potential()*self.ne

            self.results['energy'] = omega_extra
            self.results['free_energy'] = omega_free

            if not self.atoms.pbc.all():
                dipole_v = self.density.calculate_dipole_moment() * Bohr
                self.log('Dipole moment: ({0:.6f}, {1:.6f}, {2:.6f}) |e|*Ang\n'
                         .format(*dipole_v))
                self.results['dipole'] = dipole_v

            if self.wfs.nspins == 2:
                magmom = self.occupations.magmom
                magmom_a = self.density.estimate_magnetic_moments(
                    total=magmom)
                self.log('Total magnetic moment: %f' % magmom)
                self.log('Local magnetic moments:')
                symbols = self.atoms.get_chemical_symbols()
                for a, mom in enumerate(magmom_a):
                    self.log('{0:4} {1:2} {2:.6f}'.format(a, symbols[a], mom))
                self.log()
                self.results['magmom'] = self.occupations.magmom
                self.results['magmoms'] = magmom_a

            self.summary()

            self.call_observers(self.scf.niter, final=True)

    def summary(self):
        self.results['ne'] = self.ne
        self.results['electrode_potential'] = self.get_electrode_potential()
        self.hamiltonian.summary(self.occupations.fermilevel, self.log)

        self.log('----------------------------------------------------------')
        self.log('Grand Potential Energy (Composed of E_tot +E_solv - mu*ne):')
        self.log('Extrpol:    %s' % (Hartree *
                                     self.hamiltonian.e_el_extrapolated +
                                     self.get_electrode_potential() * self.ne))
        self.log('Free:    %s' % (Hartree *
                                  self.hamiltonian.e_el_free +
                                  self.get_electrode_potential() * self.ne))
        self.log('-----------------------------------------------------------')

        self.density.summary(self.atoms, self.occupations.magmom, self.log)
        self.occupations.summary(self.log)
        self.wfs.summary(self.log)
        self.log.fd.flush()
        if self.verbose:
            write_parallel_func_in_z(self.density.finegd,
                                     self.hamiltonian.vHt_g,
                                     'elstat_potential.out')

    def define_jellium(self, atoms):
        """Module for the definition of the explicit and counter charge

        """

        if 'start' in self.dl:
            if self.dl['start'] == 'cavity_like':
                pass
            elif isinstance(self.dl['start'], numbers.Real):
                pass
            else:
                raise("The starting z value of the counter charge has to be \
                      either a number (coordinate), 'cavity_like'\
                      or not given (default: max(position)+3)")
        else:
            self.dl['start'] = max(atoms.positions[:, 2]) + 3.

        if 'upper_limit' in self.dl:
            pass
        elif 'thickness' in self.dl:
            if self.dl['start'] == 'cavity_like':
                raise('With a cavity-like counter charge only the keyword \
                      upper_limit(not thickness) can be used.')
            else:
                self.dl['upper_limit'] = self.dl['start'] + \
                    self.dl['thickness']
        else:
            self.dl['upper_limit'] = self.atoms.cell[2][2]-5.

        if self.dl['start'] == 'cavity_like':

            # XXX This part can definitely be improved
            if self.hamiltonian is None:
                filename = self.log.fd
                self.log.fd = None
                self.initialize(atoms)
                self.set_positions(atoms)
                self.log.fd = filename
                g_g = self.hamiltonian.cavity.g_g.copy()
                self.wfs = None
                self.density = None
                self.hamiltonian = None
                self.initialized = False
                return CavityShapedJellium(self.ne, g_g=g_g,
                                           z2=self.dl['upper_limit'])

            else:
                filename = self.log.fd
                self.log.fd = None
                self.set_positions(atoms)
                self.log.fd = filename
                return CavityShapedJellium(self.ne,
                                           g_g=self.hamiltonian.cavity.g_g,
                                           z2=self.dl['upper_limit'])

        elif isinstance(self.dl['start'], numbers.Real):
            return JelliumSlab(self.ne, z1=self.dl['start'],
                               z2=self.dl['upper_limit'])

    def get_electrode_potential(self):
        ham = self.hamiltonian
        fermilevel = self.occupations.fermilevel
        try:
            correction = ham.poisson.correction
        except AttributeError:
            wf2 = -fermilevel
        else:
            wf2 = (-fermilevel - correction) * Hartree

        return wf2  # refpot-E_f

    def set_jellium(self, atoms):
        if abs(self.previous_ne - self.ne) > 2*self.tiny:
            if abs(self.ne) < self.tiny:
                self.ne = self.tiny
            self.set(background_charge=self.define_jellium(atoms))
