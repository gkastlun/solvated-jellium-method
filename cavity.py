import numpy as np
from ase.units import Hartree, Bohr
from gpaw.solvation.cavity import (Potential, Power12Potential,
                                   get_pbc_positions)


class SJM_Power12Potential(Power12Potential):
    """Inverse power law potential.

    An 1 / r ** 12 repulsive potential
    taking the value u0 at the atomic radius.

    See also
    A. Held and M. Walter, J. Chem. Phys. 141, 174108 (2014).
    """
    depends_on_el_density = False
    depends_on_atomic_positions = True

    def __init__(self, atomic_radii, u0, pbc_cutoff=1e-6, tiny=1e-10,
                 H2O_layer=False, unsolv_backside=True):
        """Constructor for the SJM_Power12Potential class.
        In SJM one also has the option of removing the solvent from the
        electrode backside and adding ghost atoms to remove the solvent
        from the electrode-water interface.

        Parameters
        ----------
        atomic_radii : float
            Callable mapping an ase.Atoms object to an iterable of atomic radii
            in Angstroms.
        u0 : float
            Strength of the potential at the atomic radius in eV.
        pbc_cutoff : float
            Cutoff in eV for including neighbor cells in a calculation with
            periodic boundary conditions.
        H2O_layer: bool or 'plane'
            Exclude the implicit solvent from the interface region between
            electrode and water,
        unsolv_backside: bool
            Exclude implicit solvent from the region behind the electrode

        """
        Potential.__init__(self)
        self.atomic_radii = atomic_radii
        self.u0 = float(u0)
        self.pbc_cutoff = float(pbc_cutoff)
        self.tiny = float(tiny)
        self.r12_a = None
        self.r_vg = None
        self.pos_aav = None
        self.del_u_del_r_vg = None
        self.atomic_radii_output = None
        self.symbols = None
        self.H2O_layer = H2O_layer
        self.unsolv_backside = unsolv_backside

    def update(self, atoms, density):
        if atoms is None:
            return False
        self.r12_a = (self.atomic_radii_output / Bohr) ** 12
        r_cutoff = (self.r12_a.max() * self.u0 / self.pbc_cutoff) ** (1. / 12.)
        self.pos_aav = get_pbc_positions(atoms, r_cutoff)
        self.u_g.fill(.0)
        self.grad_u_vg.fill(.0)
        na = np.newaxis

        if self.unsolv_backside:
            # Removing solvent from electrode backside
            for z in range(self.u_g.shape[2]):
                if (self.r_vg[2, 0, 0, z] - atoms.positions[:, 2].min() /
                        Bohr < 0):
                    self.u_g[:, :, z] = np.inf
                    self.grad_u_vg[:, :, :, z] = 0

        if self.H2O_layer:
            # Add ghost coordinates and indices to pos_aav dictionary if
            # a water layer is present
            oxygen = atoms.copy()
            oxygen_ind = [atom.index for atom in oxygen if atom.symbol == 'O']
            oxygen_del = [atom.index for atom in oxygen if atom.symbol != 'O']

            # Disregard oxygens that don't belong to the water layer
            for i, ox in enumerate(oxygen_ind):
                nH = 0

                for atm in atoms:
                    dist = atm.position - oxygen[ox].position
                    if np.linalg.norm(dist) < 1.1 and atm.symbol == 'H':
                        nH += 1

                if nH < 2:
                    oxygen_del.append(ox)

            oxygen_del.sort()
            oxygen_del.reverse()
            for index in oxygen_del:
                del oxygen[index]

            # Add the virtual plane

            # The value 2.25 is an empirical one and maybe should be
            # interchangable
            if self.H2O_layer == 'plane':
                plane_z = oxygen.positions[:, 2].min() - 2.25

                r_diff_zg = self.r_vg[2, :, :, :] - plane_z / Bohr
                r_diff_zg[r_diff_zg < self.tiny] = self.tiny
                r_diff_zg = r_diff_zg ** 2
                u_g = self.r12_a[oxygen_ind[0]] / r_diff_zg ** 6
                self.u_g += u_g
                u_g /= r_diff_zg
                r_diff_zg *= u_g
                self.grad_u_vg[2, :, :, :] += r_diff_zg

            else:
                # Ghost atoms instead of a plane
                O_layer = []
                for i in np.linspace(0, atoms.cell[0].max() / Bohr, num=10):
                    for j in np.linspace(0, atoms.cell[1].max() /
                                         Bohr, num=10):
                        for k in (np.linspace((oxygen.positions[:, 2].min() -
                                  5.) / Bohr, (oxygen.positions[:, 2].min() -
                                  2.7) / Bohr, num=10)):
                            O_layer.append([i, j, k])

                for ox in oxygen.positions:
                    O_layer.append((ox[0], ox[1], ox[2] - 2. *
                                    self.r12_a[oxygen_ind[0]]))

                r12_add = []
                for i in range(len(O_layer)):
                    self.pos_aav[len(atoms) + i] = [O_layer[i]]
                    r12_add.append(self.r12_a[oxygen_ind[0]])
                r12_add = np.array(r12_add)
                # r12_a must have same dimensions as pos_aav items
                self.r12_a = np.concatenate((self.r12_a, r12_add))

        for index, pos_av in self.pos_aav.items():
                pos_av = np.array(pos_av)
                r12 = self.r12_a[index]
                for pos_v in pos_av:
                    origin_vg = pos_v[:, na, na, na]
                    r_diff_vg = self.r_vg - origin_vg
                    r_diff2_g = (r_diff_vg ** 2).sum(0)
                    r_diff2_g[r_diff2_g < self.tiny] = self.tiny
                    u_g = r12 / r_diff2_g ** 6
                    self.u_g += u_g
                    u_g /= r_diff2_g
                    r_diff_vg *= u_g[na, ...]
                    self.grad_u_vg += r_diff_vg

        self.u_g *= self.u0 / Hartree
        self.grad_u_vg *= -12. * self.u0 / Hartree
        self.grad_u_vg[self.grad_u_vg < -1e20] = -1e20
        self.grad_u_vg[self.grad_u_vg > 1e20] = 1e20

        return True
